/**
 * Inicio del app desde el lado de ios
 */

// librerias
import React from 'react';
import {
  AppRegistry
} from 'react-native';

import { MainNav } from './common/components/Router';

// clase que se usa para iniciar el app
const AppCreativeria = () => (
  <MainNav />
);

// instanciacion del app
AppRegistry.registerComponent('AppCreativeria', () => AppCreativeria);
