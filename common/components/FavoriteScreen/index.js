import React, {
  Component,
} from 'react';
import {
  ListView,
  Text,
  View
} from 'react-native';

import Button from '../lib/button';
import firebaseApp from '../../database';
import styles from './style';
import { navigator } from '../Router';

export default class CompetitionsListScreen extends Component {
  constructor(props) {
    super(props);

    this.itemsRef = firebaseApp.database().ref(props.params.dataRef);

    const ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    });

    this.state = {
      dataSource: ds.cloneWithRows([false])
    };
  }

  componentDidMount() {
    this.onChangeListener = (snap) => {
      const items = [];
      snap.forEach((child) => {
        items.push({
          caption: child.val().caption,
          _key: child.key
        });
      });
      this.setState({
        dataSource: this.state.dataSource.cloneWithRows(
          JSON.parse(
            JSON.stringify(
              items || []
            )
          )
        )
      });
    };
    this.itemsRef.on('value', this.onChangeListener);
  }

  componentWillUnmount() {
    this.itemsRef.off('value', this.onChangeListener);
  }

  render() {
    return (
      <View
        style={styles.mainView}
      >
        <View
          style={styles.topTitle}
        >
          <Text>
            Listado de {this.props.params.title} favoritos
          </Text>
          <Button
            onPress={() => {
              navigator.pop();
            }}
          >
            <Text>
              Volver
            </Text>
          </Button>
        </View>
        <ListView
          enableEmptySections
          dataSource={this.state.dataSource}
          renderRow={(item) => {
            if (!item) {
              return (
                <View
                  style={styles.listItemViewNoContent}
                >
                  <Text>Cargando</Text>
                </View>
              );
            }
            return (
              <View
                style={styles.listItemView}
              >
                <Text>{item.caption}</Text>
                <Button
                  onPress={() => {
                    this.itemsRef.child(item._key).remove();
                  }}
                >
                  <Text>Borrar</Text>
                </Button>
              </View>
            );
          }}
        />
      </View>
    );
  }
}
CompetitionsListScreen.propTypes = {
  // goTo: React.PropTypes.func.isRequired,
  params: React.PropTypes.object.isRequired
};
