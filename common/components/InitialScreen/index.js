import React, {
  Component,
} from 'react';
import {
  Text,
  View
} from 'react-native';

import Button from '../lib/button';

import styles from './styles';

export default class InitialScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    return (
      <View
        style={styles.mainView}
      >
        <Button
          onPress={() => {
            this.props.goTo('CompetitionsListScreen');
          }}
        >
          <View
            style={styles.button}
          >
            <Text>
              Ver Listado de Torneos
            </Text>
          </View>
        </Button>
        <Button
          onPress={() => {
            this.props.goTo('FavoriteScreen', {
              dataRef: 'Competitions',
              title: 'torneos'
            });
          }}
        >
          <View
            style={styles.button}
          >
            <Text>
              Listado de Torneos favoritos
            </Text>
          </View>
        </Button>
        <Button
          onPress={() => {
            this.props.goTo('FavoriteScreen', {
              dataRef: 'Fixtures',
              title: 'partidos'
            });
          }}
        >
          <View
            style={styles.button}
          >
            <Text>
              Listado de Partidos favoritos
            </Text>
          </View>
        </Button>
      </View>
    );
  }
}
InitialScreen.propTypes = {
  goTo: React.PropTypes.func.isRequired
};
