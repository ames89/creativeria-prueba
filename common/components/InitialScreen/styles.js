import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  mainView: {
    marginTop: 25
  },
  button: {
    borderColor: 'black',
    borderStyle: 'solid',
    borderWidth: 1,
    padding: 4
  }
});
