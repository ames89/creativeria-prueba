import React, {
  Component,
} from 'react';
import {
  ListView,
  Text,
  View
} from 'react-native';

import { navigator } from '../Router';
import Button from '../lib/button';
import firebaseApp from '../../database';
import styles from './style';

class FixturesListScreen extends Component {
  constructor(props) {
    super(props);

    this.itemsRef = firebaseApp.database().ref('Fixtures');

    const ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    });

    this.state = {
      dataSource: ds.cloneWithRows([false]),
      favorites: []
    };
  }

  componentDidMount() {
    this.onChangeListener = (snap) => {
      const items = [];
      snap.forEach((child) => {
        items.push({
          id: child.val().id,
          _key: child.key
        });
      });
      this.setState({
        favorites: items,
        dataSource: this.state.dataSource.cloneWithRows(
          JSON.parse(
            JSON.stringify(
              this.apiData || []
            )
          )
        )
      });
      this.forceUpdate();
    };

    this.itemsRef.on('value', this.onChangeListener);

    fetch(`https://api.football-data.org/v1/competitions/${this.props.params.competition.id}/fixtures`, {
      headers: {
        'X-Response-Control': 'minified'
      }
    })
    .then(res => res.json())
    .then((data) => {
      this.apiData = data.fixtures;
      this.setState({
        dataSource: this.state.dataSource.cloneWithRows(data.fixtures)
      });
    });
  }

  componentWillUnmount() {
    this.itemsRef.off('value', this.onChangeListener);
  }

  render() {
    return (
      <View
        style={styles.mainView}
      >
        <View
          style={styles.topTitle}
        >
          <Text>
            Listado de Partidos del torneo {'\n'}{this.props.params.competition.caption}
          </Text>
          <Button
            onPress={() => {
              navigator.pop();
            }}
          >
            <Text>
              Volver
            </Text>
          </Button>
        </View>
        <ListView
          enableEmptySections
          dataSource={this.state.dataSource}
          renderRow={(item) => {
            if (!item) {
              return (
                <View
                  style={styles.listItemViewNoContent}
                >
                  <Text>Cargando</Text>
                </View>
              );
            }
            return (
              <View
                style={styles.listItemView}
              >
                <Text>{item.homeTeamName} vs {item.awayTeamName}</Text>
                <Button
                  onPress={() => {
                    const itemFav = this.state.favorites.find(fav => item.id === fav.id);
                    if (!itemFav) {
                      this.itemsRef.push({
                        id: item.id,
                        caption: `${item.homeTeamName} vs ${item.awayTeamName}`
                      });
                    } else {
                      this.itemsRef.child(itemFav._key).remove();
                    }
                  }}
                >
                  <Text
                    style={[
                      !!this.state.favorites.find(fav => item.id === fav.id) &&
                      styles.itemFavorited
                    ]}
                  >Favorito</Text>
                </Button>
              </View>
            );
          }}
        />
      </View>
    );
  }
}
FixturesListScreen.propTypes = {
  params: React.PropTypes.object.isRequired
};
export default FixturesListScreen;
