import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  topTitle: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  listItemView: {
    borderColor: 'black',
    borderStyle: 'solid',
    borderWidth: 1,
    padding: 4,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  listItemViewNoContent: {
    borderColor: 'black',
    borderStyle: 'solid',
    borderWidth: 1,
    padding: 4
  },
  itemFavorited: {
    color: 'red'
  },
  mainView: {
    marginTop: 25
  },
});
