import React, { Component } from 'react';
import { Navigator } from 'react-native';

// pantallas
import InitialScreen from '../InitialScreen';
import CompetitionsListScreen from '../CompetitionsListScreen';
import FixturesListScreen from '../FixturesListScreen';
import FavoriteScreen from '../FavoriteScreen';

let navigator;

const methods = {};

class MainNav extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <Navigator
        initialRoute={{
          name: 'InitialScreen',
          index: 0,
        }}

        configureScene={(route) => {
          if (route.sceneConfig) {
            return route.sceneConfig;
          }
          return Navigator.SceneConfigs.PushFromRight;
        }}

        renderScene={
          (route, _navigator) => {
            navigator = _navigator;
            methods.onForward = (sceneName, params, options) => {
              if (!options) {
                options = {};
              }
              const nextIndex = route.index + 1;
              navigator.push({
                name: sceneName,
                params,
                index: nextIndex,
                sceneConfig: options.transition
              });
            };
            methods.resetTo = (sceneName, params, options) => {
              if (!options) {
                options = {};
              }
              navigator.resetTo({
                name: sceneName,
                params,
                index: 0,
                sceneConfig: options.transition
              });
            };
            switch (route.name) {
              case 'InitialScreen':
                return (
                  <InitialScreen
                    goTo={methods.onForward}
                    resetTo={methods.resetTo}
                  />
                );

              case 'CompetitionsListScreen':
                return (
                  <CompetitionsListScreen
                    goTo={methods.onForward}
                    resetTo={methods.resetTo}
                  />
                );

              case 'FixturesListScreen':
                return (
                  <FixturesListScreen
                    goTo={methods.onForward}
                    resetTo={methods.resetTo}
                    params={route.params}
                  />
                );

              case 'FavoriteScreen':
                return (
                  <FavoriteScreen
                    goTo={methods.onForward}
                    resetTo={methods.resetTo}
                    params={route.params}
                  />
                );

              default:
                return (
                  <InitialScreen
                    goTo={methods.onForward}
                    resetTo={methods.resetTo}
                  />
                );
            }
          }
        }
      />
    );
  }
}

export { navigator, MainNav };
