import React from 'react';
import {
  TouchableOpacity
} from 'react-native';


/**
 * Boton
 * Clase que tiene como funcion ser un boton con vista configurable
 * @class Button
 * @extends {React.Component}
 */
class Button extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <TouchableOpacity onPress={this.props.onPress}>
        {this.props.children}
      </TouchableOpacity>
    );
  }
}
// tipos de parámetros
Button.propTypes = {
  children: React.PropTypes.element.isRequired,
  onPress: React.PropTypes.func.isRequired
};

export default Button;
