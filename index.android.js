/**
 * Inicio del app desde el lado de android
 */

// librerias
import React from 'react';
import {
  AppRegistry,
  BackAndroid
} from 'react-native';

import { navigator, MainNav } from './common/components/Router';

// el boton de retroceso de android
BackAndroid.addEventListener('hardwareBackPress', () => {
  if (navigator && navigator.getCurrentRoutes().length > 1) {
    navigator.pop();
    return true;
  }
  return false;
});

// clase que se usa para iniciar el app
const AppCreativeria = () => (
  <MainNav />
);

// instanciacion del app
AppRegistry.registerComponent('AppCreativeria', () => AppCreativeria);
